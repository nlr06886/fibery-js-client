[Fibery API documentation](https://fibery.gitlab.io/api-docs/?javascript) with examples using this client.

## Getting started

```javascript
const Fibery = require('fibery-unofficial');
const fibery = new Fibery({
    host: 'YOUR_ACCOUNT.fibery.io',
    token: 'YOUR_TOKEN'
});
```


## Method-way vs. command-way

This unofficial Node.js client supports two ways of working.
Method-way is used in the [API documentation](https://fibery.gitlab.io/api-docs/?javascript):

```javascript
const users = await fibery.entity.query({
  'q/from': 'fibery/user',
  'q/select': ['fibery/id', 'user/name'],
  'q/limit': 3
});
```

It is equivalent to command-way:

```javascript
const usersCommand = fibery.command.queryEntityCmd({
  'q/from': 'fibery/user',
  'q/select': ['fibery/id', 'user/name'],
  'q/limit': 3
});

const users = await fibery.command.execute(usersCommand);
```

The method-way utilizes commands under the hood, but provides a more simple interface.

The command-way removes an abstraction layer, making it closer to the actual API but also less suitable for a one-time use.

Feel free to use any of the ways.


## Differences from API

This client makes it easier to:

* [Create domain Type](https://fibery.gitlab.io/api-docs/?javascript#create-type) — creates five mandatory primitive Fields and installs rank mixin. 
* [Create relation (entity [collection] Field)](https://fibery.gitlab.io/api-docs/?javascript#relation-entity-collection-field) — creates both entity Fields and connects them together.
* [Create single-select Field](https://fibery.gitlab.io/api-docs/?javascript#single-select-field) — creates an auxiliary Type and options as Entities of that Type, makes selection required and sets the default option.


## Examples

* A bunch of [short examples](https://gitlab.com/fibery-community/api-examples/blob/master/short-examples.js).
* [Migration from Airtable](https://gitlab.com/fibery-community/api-examples/tree/master/migrate-from-airtable) prototype. 