const Type = require("./type");
const Field = require("./field");
const Entity = require("./entity");
const Network = require("./network");

const Command = class {
  constructor(host, token) {
    this._host = host;
    this._token = token;
    this._commandsEndpoint = "/api/commands";

    this.createTypeBatchCmd = Type.commands.createTypeBatchCmd;
    this.renameTypeBatchCmd = Type.commands.renameTypeBatchCmd;
    this.deleteTypeBatchCmd = Type.commands.deleteTypeBatchCmd;

    this.createFieldBatchCmd = Field.commands.createFieldBatchCmd;
    this.renameFieldBatchCmd = Field.commands.renameFieldBatchCmd;
    this.deleteFieldBatchCmd = Field.commands.deleteFieldBatchCmd;

    this.queryEntityCmd = Entity.commands.queryEntityCmd;
    this.createEntityBatchCmds = Entity.commands.createEntityBatchCmds;
    this.updateEntityBatchCmds = Entity.commands.updateEntityBatchCmds;
    this.addToEntityCollectionFieldBatchCmds =
      Entity.commands.addToEntityCollectionFieldBatchCmds;
    this.removeFromEntityCollectionFieldBatchCmds =
      Entity.commands.removeFromEntityCollectionFieldBatchCmds;
    this.deleteEntityBatchCmds = Entity.commands.deleteEntityBatchCmds;
  }

  executeBatch(commands) {
    let url = "https://" + this._host + this._commandsEndpoint;
    return Network.post(url, {}, this._token, commands)
      .then((response) => {
        return response.map(res => res.result);
      })
      .catch((err) => {
        throw new Error(err);
      });
  }

  execute(command) {
    return this.executeBatch([
      {
        command: command.command,
        args: command.args,
      },
    ]).then((results) => results[0]);
  }
};

module.exports = Command;
