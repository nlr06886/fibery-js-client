const Command = require('./command');
const Type = require('./type');
const Field = require('./field');
const Entity = require('./entity');
const File = require('./file');
const Document = require('./document');


const Fibery = class {
    constructor(options) {
        if (!options.host) {
            throw new Error('Please provide a host (*.fibery.io)');
        }

        if (!options.token) {
            throw new Error('Please provide an API token');
        }

        this.command = new Command(options.host, options.token);

        this.type = {
            DOMAIN_TYPE_FIELDS: Type.meta.DOMAIN_TYPE_FIELDS,
            createBatch: async (argsArray) => this.command.execute(Type.commands.createTypeBatchCmd(argsArray)),
            renameBatch: async (argsArray) => this.command.execute(Type.commands.renameTypeBatchCmd(argsArray)),
            deleteBatch: async (argsArray) => this.command.execute(Type.commands.deleteTypeBatchCmd(argsArray))
        };

        this.field = {
            PRIMITIVE_FIELD_TYPES: Field.meta.PRIMITIVE_FIELD_TYPES,
            SYNTHETIC_FIELD_TYPES: Field.meta.SYNTHETIC_FIELD_TYPES,
            createBatch: async (argsArray) => this.command.execute(Field.commands.createFieldBatchCmd(argsArray)),
            renameBatch: async (argsArray) => this.command.execute(Field.commands.renameFieldBatchCmd(argsArray)),
            deleteBatch: async (argsArray) => this.command.execute(Field.commands.deleteFieldBatchCmd(argsArray))
        };

        this.entity = {
            query: async (query, params = {}) => this.command.execute(Entity.commands.queryEntityCmd(query, params)),
            createBatch: async (argsArray) => this.command.executeBatch(Entity.commands.createEntityBatchCmds(argsArray)),
            updateBatch: async (argsArray) => this.command.executeBatch(Entity.commands.updateEntityBatchCmds(argsArray)),
            addToEntityCollectionFieldBatch: async (argsArray) => this.command.executeBatch(Entity.commands.addToEntityCollectionFieldBatchCmds(argsArray)),
            removeFromEntityCollectionFieldBatch: async (argsArray) => this.command.executeBatch(Entity.commands.removeFromEntityCollectionFieldBatchCmds(argsArray)),
            deleteBatch: async (argsArray) => this.command.executeBatch(Entity.commands.deleteEntityBatchCmds(argsArray))
        };

        this.file = new File(options.host, options.token);
        this.document = new Document(options.host, options.token);
    }

    getSchema() {
        return this.command.execute({command: 'fibery.schema/query'})
            .then(result => result['fibery/types']);
    }
};

module.exports = Fibery;
