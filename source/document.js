const Network = require('./network');


const Document = class {
    constructor(host, token) {
        this._host = host;
        this._token = token;
        this._endpoint = '/api/documents';
        this._batchEndpoint = '/api/documents/commands';
        this.FORMATS = ['md', 'html', 'json'];
    }

    _headers(){
        return {
            'Authorization': `Token ${this._token}`,
            'X-Client': 'Unofficial JS'
        }
    }

    get(secret, format = 'md') {
        if (!secret) {
            throw new Error('Get Document. Secret is missing.');
        }

        if (!this.FORMATS.includes(format)) {
            throw new Error(`Get Document. '${format}' format is not supported yet. Try ${this.FORMATS.join(',')}`);
        }

        let url = `https://${this._host}${this._endpoint}/${secret}`
        return Network.get(url, { format }, this._token)
    }

    getBatch(secrets, format = 'md') {
        if (!this.FORMATS.includes(format)) {
            throw new Error(`Get Documents. '${format}' format is not supported yet. Try ${this.FORMATS.join(',')}`);
        }

        let url = `https://${this._host}${this._batchEndpoint}`
        let data = {
            command: 'get-documents',
            args: secrets
        }
        return Network.put(url, { format }, this._token, data)
    }

    update(secret, content, format = 'md') {
        if (!secret) {
            throw new Error('Get Document. Secret is missing.');
        }

        if (!this.FORMATS.includes(format)) {
            throw new Error(`Get Document. '${format}' format is not supported yet. Try ${this.FORMATS.join(',')}`);
        }

        let url = `https://${this._host}${this._endpoint}/${secret}`
        return Network.put(url, { format: format }, this._token, { content: content })
    }
};


module.exports = Document;