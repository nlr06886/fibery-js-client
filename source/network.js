const fetch = require("isomorphic-fetch");


const commonRequestOptions = {
  mode: "cors", // no-cors, *cors, same-origin
  cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
  credentials: "same-origin", // include, *same-origin, omit,
  redirect: "follow", // manual, *follow, error
  referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
};


function getHeaders(token) {
  return {
    "Content-Type": "application/json",
    Authorization: `Token ${token}`,
    "X-Client": "Unofficial JS",
  };
}


function buildUrl(url, params){
    var url = new URL(url);
    url.search = new URLSearchParams(params).toString();
    return url.toString();
}


async function request(method, url, params = {}, token, data) {
    url = buildUrl(url, params);

    let requestParams = {
        method: method,
        headers: getHeaders(token),
        ...commonRequestOptions
    }

    if (data) {
        requestParams.body = JSON.stringify(data)
    }

    let response = await fetch(url, requestParams);
    let content = await response.json();
    return content;
}


async function get(url, params = {}, token) {
    let content = await request('GET', url, params, token);
    return content;
}


async function put(url, params = {}, token, data = {}) {
  let content = await request('PUT', url, params, token, data);
  return content;
}


async function post(url, params = {}, token, data = {}) {
  let content = await request('POST', url, params, token, data);
  return content;
}


module.exports = {
    get,
    put,
    post
}