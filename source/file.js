const request = require('isomorphic-fetch');
const fs = require('fs');


const File = class {
    constructor(host, token) {
        this._host = host;
        this._token = token;
        this._filesEndpoint = '/api/files'
    }

    upload(path) {
        if (!path) {
            throw new Error('Upload File. Path is missing.');
        }

        return request.post({
            url: 'https://' + this._host + this._filesEndpoint,
            headers: {
                'Authorization': `Token ${this._token}`,
                'X-Client': 'Unofficial JS'
            },
            formData: {
                file: fs.createReadStream(path)
            }
        })
    }

    download(secret, destination) {
        if (!secret) {
            throw new Error('Download File. Secret is missing.');
        }

        if (!destination) {
            throw new Error('Download File. Destination is missing.');
        }

        return request.get({
            url: 'https://' + this._host + this._filesEndpoint + `/${secret}`,
            headers: {
                'Authorization': `Token ${this._token}`,
                'X-Client': 'Unofficial JS'
            },
            encoding: 'binary'
        }).then(file => fs.promises.writeFile(destination, file, 'binary'));
    }
};


module.exports = File;